#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 21:18:08 2017

@author: mrigesh
"""

from eve import Eve

app = Eve()


def get_unique_number(phone_number):
    phone_number = phone_number
    count = 0
    doc = app.data.driver.db['phone_number']
    data = doc.find({'phone_number':phone_number})
    count = data.count()
    if not count:
        return phone_number
    else:
        phone_number += 1
        if phone_number > 9999999999:
            phone_number = 1111111111
        unique_number = get_unique_number(phone_number)
        return unique_number

def before_insert(resource_name, items):
    for item in items:
        phone_number = item.get('phone_number')
        unique_number = get_unique_number(phone_number)
        item['phone_number'] = unique_number

app.on_insert += before_insert

if __name__ == '__main__':
    app.run()