#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 21:32:13 2017

@author: mrigesh
"""
MONGO_DBNAME = 'zyla_phone'

schema = {
    # Schema definition, based on Cerberus grammar. Check the Cerberus project
    # (https://github.com/nicolaiarocci/cerberus) for details.
    'firstname': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 10,
        'required': True,
    },
    'lastname': {
        'type': 'string',
        'minlength': 1,
        'maxlength': 15,
        'required': True,
    },
    'gender': {
        'type': 'string',
        'allowed': ["male", "female"],
        'required': False
    },
    'location': {
        'type': 'dict',
        'schema': {
            'address': {'type': 'string'},
            'city': {'type': 'string'}
        },
        'required': False
    },
    'dob': {
        'type': 'datetime',
        'required': False,
    },
    'phone_number': {
        'type': 'integer',
        'min': 1111111111,
        'max': 9999999999,
        'required': False,
        'unique': False,
    },
}


people = {
    'item_title': 'person',

    # by default the standard item entry point is defined as
    # '/people/<ObjectId>'. We leave it untouched, and we also enable an
    # additional read-only entry point. This way consumers can also perform
    # GET requests at '/people/<lastname>'.
    'additional_lookup': {
        'url': 'regex("[0-9]{10}")',
        'field': 'phone_number'
    },

    # We choose to override global cache-control directives for this resource.
    'cache_control': 'max-age=10,must-revalidate',
    'cache_expires': 10,

    # most global settings can be overridden at resource level
    'resource_methods': ['GET', 'POST'],

    'schema': schema,

    'id_field': 'phone_number',
}

DOMAIN = {'phone_number': people}
